/*

WELCOME TO THE DARK SIDE

"If you are reading this then this warning is for you. Every word you read of this useless fine print is another second off your life. 
Don't you have other things to do? Is your life so empty that you honestly can't think of a better way to spend these moments? Or are you so 
impressed with authority that you give respect and credence to all that claim it? Do you read everything you're supposed to read? 
Do you think every thing you're supposed to think? Buy what you're told to want? Get out of your apartment. Meet a member of the opposite sex. 
Stop the excessive shopping and masturbation. Quit your job. Start a fight. Prove you're alive. If you don't claim your humanity you will 
become a statistic. You have been warned." - Chuck Palahniuk, Fight Club


--

Design & Development
Kurtulus Birgoren
kbirgoren@gmail.com
https://www.linkedin.com/in/kbirgoren/


*/                                                

var client_id = 'UEHEPLAPPBFUUQQWD0WRVLVPRD4RD4CHFDPHZ40YJ4OBS1SL'
var client_secret = '3KGADOPCKU0XCHI5ITIA5OYPTNTXTF3TR12LRCFPE0ILM1GX';

var today = new Date();
var date = today.toISOString().substring(0, 10).replace(/-/g, '');
var longitude, latitude
var radius = 150;


function get_location() {
	if ("geolocation" in navigator) {
		navigator.geolocation.getCurrentPosition(function(position) {
			longitude = position.coords.longitude;
			latitude = position.coords.latitude;
			api_call(longitude, latitude, radius);
			$("#header h4 span.turn_on").fadeOut();
			$("#header h4 span.wait").fadeIn();
		}.bind(this), function(error) {
			alert("Please turn on location for this feature")
			$("#header a").slideUp();
			$("#header h4 span.turn_on,#header h4 span.wait").fadeOut();
			$("#header h4 span.denied").fadeIn();
		});
	} else {
		alert("Browser doesn't support geolocation!");
	}
}

function api_call(longitude, latitude, radius) {
	$.ajax({
		url: 'https://api.foursquare.com/v2/venues/explore',
		dataType: 'json',
		method: 'get',
		headers: {
			'Accept-Language': 'en',
		},
		data: '&offset=0&limit=60&ll=' + latitude + ',' + longitude + '&client_id=' + client_id + '&client_secret=' + client_secret + '&radius=' + radius + '&venuePhotos=1' + '&v=' + date,
		success: function(data) {
			$("#header").addClass("h400");
			if ($("#header .content").css("display") !== "none") {
				$("#header .content").fadeOut("fast");
				$("#header .results").css("display", "flex").hide().fadeIn("slow");
			}
			setTimeout(function() {
				$("#header").css("position", "relative")
				var items = data['response']['groups'][0]['items'];
				var item_count = data['response']['totalResults']
				$('#places').removeClass("loading")
				for (var item in items) {
					//alert(venue)
					$('#places').append('<li class="venue"><div class="content"></div></li>')
					item_index = (Object.keys(items).indexOf(item));
					venue = items[item]['venue'];
					venue_name = venue['name'].replace(/\(.*?\)/g, "");
					venue_icon = venue['categories'][0]['icon']['prefix'] + '64' + venue['categories'][0]['icon']['suffix'];
					venue_cat = venue['categories'][0]['shortName']
					venue_address = venue['location']['address']
					venue_distance = venue['location']['distance'];
					venue_washere = venue['stats']['usersCount']
					if ("featuredPhotos" in venue) {
						venue_photo = venue['featuredPhotos']['items'][0]['prefix'] + '700x700' + venue['photos']['groups'][0]['items'][0]['suffix']
						$("#places li").eq(item_index).append('<div class="bg" style="background:url(' + venue_photo + ') center center;background-size: cover;"></div><div class="bg_overlay"></div>')
					}
					content = $("#places li").eq(item_index).find(".content");
					if ("rating" in venue) {
						venue_rating = venue['rating'];
						venue_rating_color = venue['ratingColor'];
						content.append('<div class="rating" style="background:#' + venue_rating_color + '">' + venue_rating + '</div>')
					}
					content.append('<div class="details"><div class="cat"><div class="icon"><img src=' + venue_icon + '	></div> ' + venue_cat + ' </div><div class="name">' + venue_name + '</div><div class="address">' + venue_address + '</div></div>').append('<div class="distance">' + venue_distance + ' M <span class="washere"> — visited by ' + venue_washere + ' people</span></div>')
				}
			}, 401);
		},
		error: function(e) {
			alert("Error with API call")
		}
	});
}
$("#header .bt").click(function(e) {
	$("#header .no").slideUp();
	$(this).addClass("loading");
	get_location();
});
$("#header .filters .distance a").click(function(e) {
	$('#places').addClass("loading").html("");
	$("#header .filters .distance a").removeClass("selected")
	$(this).addClass("selected");
	new_radius = $(this).data("meters")
	fill_width = parseInt($(this).css("left").replace("px", "")) + 30;
	$("#header .filters .bar .fill").width(fill_width + "px")
	$("#header .results h4 span").text(new_radius)
	api_call(longitude, latitude, new_radius)
});